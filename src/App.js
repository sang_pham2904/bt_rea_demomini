import logo from "./logo.svg";
import "./App.css";
import DemoRedux from "./DemoRedux/DemoRedux";

function App() {
  return (
    <div className="App">
      <DemoRedux />
    </div>
  );
}

export default App;
