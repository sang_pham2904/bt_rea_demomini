let initialState = {
  number: 1,
};
export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case "TANG_SO": {
      let buttonGiam = document.getElementById("btn_giam");
      buttonGiam.disabled = false;
      let cloneState = { ...state };
      cloneState.number++;
      return cloneState;
    }
    case "GIAM_SO": {
      let buttonGiam = document.getElementById("btn_giam");
      let so = document.getElementById("soLuong").innerText;
      console.log(so);
      let cloneState = { ...state };

      if (so > 1) {
        cloneState.number--;

        buttonGiam.disabled = false;
      } else {
        buttonGiam.disabled = true;
      }
      return cloneState;
    }

    default: {
      return state;
    }
  }
};
