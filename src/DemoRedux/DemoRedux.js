import React, { Component } from "react";
import { connect } from "react-redux";
class DemoRedux extends Component {
  // check = () => {
  //   let so = document.getElementById("soLuong").innerText;
  //   let buttonGiam = document.getElementById("btn_giam");
  //   if (so <= 1) {
  //     buttonGiam.ariaDisabled = true;
  //   }
  // };
  render() {
    // this.check();
    return (
      <div>
        <button
          id="btn_giam"
          className="btn btn-danger"
          onClick={() => this.props.handleGiam(-2)}
        >
          -
        </button>
        <strong id="soLuong" className="p-2">
          {" "}
          {this.props.currentNumber}{" "}
        </strong>
        <button className="btn btn-primary" onClick={this.props.handleTang}>
          +
        </button>
      </div>
    );
  }
}
let mapStateToProp = (state) => {
  return { currentNumber: state.soLuong.number };
};
let mapDispatchToProp = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG_SO",
      };
      dispatch(action);
    },
    handleGiam: (num) => {
      let action = {
        type: "GIAM_SO",
        payload: num,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProp, mapDispatchToProp)(DemoRedux);
